/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belajar.web.service.catalog.controller;

import com.belajar.web.service.catalog.dao.ProductDao;
import com.belajar.web.service.catalog.entity.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author dans
 */
@RestController @RequestMapping("api/product")   
public class ProductApiController {
    private static final Logger logger = LoggerFactory.getLogger(ProductApiController.class);
    
    @Autowired
    private ProductDao productDao;

    @GetMapping("/")
    public Page<Product> findProduct(Pageable page){
        return productDao.findAll(page);
    }

    @GetMapping("/{id}")
    public Product findProductByID(@PathVariable("id") Product product){
        return product;
    }

}
